/**
 * Created by Kyrylo Havrylenko on 01.01.2016.
 */


$(document).ready(function () {

    $("#backButton").click(function () {
        window.location.href = "/";
    });

    function Timer() {

        this.currentDate = null;
        this.warcraftDate = null;
        this.legionDate = new Date(2016, 08, 21);
        this.overwatchDate = new Date(2016, 05, 21);
    }

    Timer.prototype.hello = function () {
        alert("Hello world");
    };

    Timer.prototype.parseDate = function (url) {

        var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest; // anti-ie
        var xhr = new XHR();

        xhr.open('GET', url, false); // true to async
        xhr.send();


            if (xhr.status != 200) {
                alert("Get request to server went wrong " + xhr.status);
            } else {

                var str = xhr.responseText;
                var obj = JSON.parse(str, function (key, value) {
                    if (key == 'Released') return new Date(value);
                    return value;
                });
            }

            return obj.Released;


    };

    Timer.prototype.createTimer = function (endDate, idDOMElement) {
        this.currentDate = new Date();
        if (this.currentDate != endDate) {
            var left = endDate - this.currentDate;
            $(idDOMElement).text("Left: " + Timer.prototype.convert(left));
            return false;
        } else {
            return true;
        }
    };

    Timer.prototype.convert = function (duration) {

        var milliseconds = parseInt((duration%1000)/100);
        var seconds = parseInt((duration/1000)%60);
        var minutes = parseInt((duration/(1000*60))%60);
        var hours = parseInt((duration/(1000*60*60))%24);
        var days = parseInt((duration/(1000*60*60*24)));

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return days + " days and " + hours + ":" + minutes + ":" + seconds;

    };



    var timer = new Timer();

    timer.warcraftDate = timer.parseDate("http://www.omdbapi.com/?i=tt0803096&plot=short&r=json");
    $("#warcraftDate").text("Date: " + timer.warcraftDate.toDateString());
    $("#overwatchDate").text("Date: " + timer.overwatchDate.toDateString());
    $("#legionDate").text("Date: " + timer.legionDate.toDateString());

    timer.createTimer(timer.warcraftDate, "#warcraftLeft");
    timer.createTimer(timer.legionDate, "#legionLeft");
    timer.createTimer(timer.overwatchDate, "#overwatchLeft");

    setInterval(function() {
        timer.createTimer(timer.warcraftDate, "#warcraftLeft");
        timer.createTimer(timer.legionDate, "#legionLeft");
        timer.createTimer(timer.overwatchDate, "#overwatchLeft");
    }, 1000);


});