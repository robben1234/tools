/**
 * Created by robben1 on 3/21/16.
 */

$(document).ready(function () {

    $("#goGet").click(function () {
        var pic = new Converter();
        var url = $("#url").val();
        pic.convert(url);
        pic.formNewUrl();
        pic.showImg();

    });

    $("#backButton").click(function () {
        window.location.href = "/";
    });

    function Converter() {

        this.originUrl = null;
        this.newUrl = null;
        this.firstPart = "http://img.youtube.com/vi/";
        this.videoCode = null;
        this.lastPart = "/maxresdefault.jpg";
    }

    Converter.prototype.convert = function (oldValue) {
        this.originUrl = oldValue;
        var patt = /(.+)v=([^\&]+)(.+)?$/i;
        var match = this.originUrl.match(patt);
        this.videoCode = match[2];
    };
    
    Converter.prototype.formNewUrl = function () {
        this.newUrl = this.firstPart + this.videoCode + this.lastPart;
    };
    
    Converter.prototype.showImg = function () {
        $("#result").remove();
        var div = $('<div>', {'id': 'result'}).appendTo('body');
        $("<h3></h3>").text(this.newUrl).appendTo(div);
        var img = $('<img />', {src:this.newUrl, style:"max-width: 100%;"});
        img.appendTo(div);
    };
})
;